# projet_calcul_routier

Un projet utilisant l'API de Google Maps afin de calculer la durée de transports entre 2 points selon certains paramètres.

Pour utiliser le fichier, il suffit juste de lancer `calcul_routier.py` avec un interpreteur python.
