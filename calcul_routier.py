import json
import requests
import datetime 

depart = str(input("Quelle est vôtre ville de départ ? .. "))
arrivee = str(input("Quelle est vôtre ville d'arrivee' ? .. "))

apiKey = 'AIzaSyDQYu3D63fl6_w6yrs5Hv1t_KglirqE8sQ'

def calcul_distance(depart, arrivee):
        ###### Récupération de la distance 
    #(j'arrive pas à utiliser .decode() pour le JSON ducoup j'ai fait un truc maison)
    req = requests.get(f'https://maps.googleapis.com/maps/api/distancematrix/json?origins={depart}&destinations={arrivee}&key={apiKey}')
    res = req.json()
    infos = []
    for cles in res.values():
        infos.append(cles)
    data = infos[2]
    data_f = ''
    for element in data:
        for values in element.values():
            for el in values:
                data_f+=(el['distance']['text'])
    data_f = data_f.replace(",",'.')        
    data_f = data_f.replace(" km",'')
    distance = float(data_f)
    return distance

def temps_transport_en_h(distance):
    ###### Algo, j'utilise l'heure comme format de temps
    vitesse_max = 90 #en km/h
    pause = 0.25 #15 min
    acc_tmps = 0.15 #9 min
    decc_tmps = 0.15 #9 min
    acc_dist = 7.5 #en km
    decc_dist = 7.5 #en km
    nb_pause = 0 
    dist_parc_max = 165 #km 
    dist_parc_max_compteur = 165

    while dist_parc_max_compteur < distance:
        nb_pause += 1
        dist_parc_max_compteur += 165
    pause_trajet = pause * nb_pause
    distance_restante = (distance - (nb_pause * dist_parc_max) - (acc_dist + decc_dist))
    duree_trajet_h = pause_trajet + (nb_pause * 2) + (distance_restante/vitesse_max)
    return duree_trajet_h


def convert(n): 
    return str(datetime.timedelta(hours = n)) 

print("La ville de départ est :", depart)
print("La ville d'arrivée est :", arrivee)
print("La distance parcourue est de", calcul_distance(depart, arrivee),"km(s)")
print("Le temps de transport estimé est de :", convert(temps_transport_en_h(calcul_distance(depart, arrivee))), "Heure, min, sec")
